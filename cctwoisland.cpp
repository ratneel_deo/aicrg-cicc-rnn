/*
NormalisedMeanSquaredError 
 */
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <ctime>


#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<ctype.h>

time_t TicTime;
time_t TocTime;
using namespace::std;

typedef vector<double> Layer;
typedef vector<double> Nodes;
typedef vector<double> Frame;
typedef vector<int> Sizes;
typedef vector<vector<double> > Weight;
typedef vector<vector<double> > Data;

const double MinimumError = 0.00001;
  

    char  SaveLearnt[15] = "SaveLearnt.txt"; //both mean the same. 
    char LearntWeights[20] = "LearntWeights.txt";// both mean the same. 


const int trainsize = 2278;  //255
const int testsize  =   865 ; //16000 
 

   char  * trainfile = "/home/rohit/Dropbox/AICRG-SoftwareReleases/Data/CycloneData/SouthPacificTracks/EmbedTrainD5T3.txt";  //  may need to update this path to your own pc 
   char * testfile= "/home/rohit//Dropbox/AICRG-SoftwareReleases/Data/CycloneData/SouthPacificTracks/EmbedTestD5T3.txt"; //  may need to update this path to your own pc 
   
 #define  neuronlevel   //hiddenNeuron, weightlevel, neuronlevel,networklevel (leave as neuronlevel for CICC)

#define sigmoid //tanh, sigmoid

int maxgen =20000;  //maximum number of function evaluations  

 const int LayersNumber = 3; //total number of layers.

 const int MaxVirtLayerSize = 10;


const double MaxErrorTollerance = 0.20;
const double MomentumRate = 0;
const double Beta = 0;
int row ;
int col;
int layer;
int r;
int x;
int y;

double weightdecay = 0.005;

 //#include "objective.h"    //objective function
  //#include "random.h"       //random number generator
 #include "RandomNumber.h"       //random number generator


#define rosen          // choose the function:
 


#define EPSILON 1e-30

#define MAXFUN 50000000  //upper bound for number of function evaluations

#define MINIMIZE 1      //set 1 to minimize and -1 to maximize
#define LIMIT 1e-20     //accuracy of best solution fitness desired
#define KIDS 2          //pool size of kids to be formed (use 2,3 or 4)
#define M 1             //M+2 is the number of parents participating in xover (use 1)
#define family 2        //number of parents to be replaced by good individuals(use 1 or 2)
#define sigma_zeta 0.1
#define sigma_eta 0.1   //variances used in PCX (best if fixed at these values)

const int  PopSize = 200;

#define  NoSpeciesCons 300
//#define  NumV 3


#define NPSize KIDS + 2   //new pop size

#define RandParent M+2     //number of parents participating in PCX


#define MAXRUN 10      //number of runs each with different random initial population

double d_not[PopSize];

double seed,basic_seed;



int RUN;



class Samples{
      ////friend class TrainingExamples;
       // friend class NeuralNetwork;
      public:

       Data  InputValues;
       Data  DataSet;
       Layer  OutputValues;
	   int PhoneSize;
      //int SampleSize;
       public:
        Samples()
        {
      //PhoneSize = 5;
        }

      };



typedef vector<Samples> DataSample;

class TrainingExamples{
     //  friend class NeuralNetwork;
  //     friend class CombinedEvolution;
   //    friend class CoEvolution;
    public:

	   char* FileName;

	    int SampleSize;
        int ColumnSize ;
        int OutputSize;
        int RowSize ;

	    DataSample Sample;

	   // Samples Sample[MaxSampSize];

    public:
    TrainingExamples(  )
    {




    } ;

    TrainingExamples( char* File, int sampleSize, int columnSize, int outputSize){


        Samples sample;

     for( int i = 0; i < sampleSize; i++){
     Sample.push_back(sample);
     }

      int rows;
      RowSize = MaxVirtLayerSize;// max number of rows.
      ColumnSize = columnSize;
      SampleSize= sampleSize;
      OutputSize = outputSize;

    ifstream in(File);

    //initialise input vectors
   for( int sample = 0; sample < SampleSize; sample++){

   for(int r=0; r <  RowSize  ; r++)
   Sample[sample].InputValues.push_back(vector<double> ());

   for(int row = 0; row < RowSize  ; row++) {
   for(int col = 0; col < ColumnSize ; col++)
      Sample[sample].InputValues[row].push_back(0);
      }

     for(int out = 0; out < OutputSize  ; out++)
        Sample[sample].OutputValues.push_back(0);
      }
   //---------------------------------------------

  for( int samp = 0; samp < SampleSize; samp++){
          in>>rows;
       Sample[samp].PhoneSize = rows;

       for( row  = 0; row   < Sample[samp].PhoneSize  ; row++) {
       for( col  = 0; col  < ColumnSize; col ++)
          in>> Sample[samp].InputValues[row ][col] ;}

       for(int out = 0; out < OutputSize  ; out++)
          in>>Sample[samp].OutputValues[out] ;

      // cout<<rows<<endl;
  }

  cout<<"printing..."<<endl;




       in.close();
        }

   void printData();



};
 //.................................................

    void TrainingExamples:: printData()
   {
       for( int sample = 0; sample < SampleSize; sample++){
    for( row  = 0; row   < Sample[sample].PhoneSize  ; row++) {
    for( col  = 0; col  < ColumnSize; col ++)
      cout<< Sample[sample].InputValues[row ][col]<<" ";
      cout<<endl;
      }
      cout<<endl;
       for(int out = 0; out < OutputSize  ; out++)
        cout<<" "<<Sample[sample].OutputValues[out]<<" ";

        cout<<endl<<"--------------"<<endl;
      }
    }


//*********************************************************
class Layers{
     // friend class NeuralNetwork;
     // friend class GeneticAlgorithmn;

      //friend class CombinedEvolution;
      public:

         // Weight Weights ;
          //Weight WeightChange;
		  //Weight ContextWeight;

     double Weights[35][35] ;
            double WeightChange[35][35];
      		  double ContextWeight[35][35];

      		  Weight TransitionProb ;


          Data RadialOutput;
          Data Outputlayer;
          Layer Bias;
          Layer BiasChange;
          Data Error;

          Layer Mean;
          Layer StanDev;

          Layer MeanChange;
          Layer StanDevChange;


       public:
        Layers()
        {

        }

      };

//***************************************************

//typedef vector<Layers> Nlayer;
//:public TrainingExamples
class NeuralNetwork: public virtual TrainingExamples {
    // friend class GeneticAlgorithmn;

    // friend class CombinedEvolution;
      public:
    // Nlayer nLayer(4);

     Layers nLayer[LayersNumber];

     double Heuristic;
     Layer ChromeNeuron;
     Data Output;
 double NMSE;
 int StringSize;

      Sizes layersize;

       // int sampleSize;
       // int columnSize;
       // int outputSize;
     public:

     NeuralNetwork(Sizes layer )
     {
      layersize  = layer;

      StringSize = (layer[0]*layer[1])+(layer[1]*layer[2])+ (layer[1]*layer[1])+   (layer[1] + layer[2]);

    //  sampleSize = SampleSize;
     // columnSize = ColumnSize;
     // outputSize = OutputSize;
     }
       NeuralNetwork()
            {


        }

   double Random();

   double SigmoidS(double ForwardOutput);
   double Sigmoid(double ForwardOutput);
     double NMSError() {return NMSE;} 

   void CreateNetwork(Sizes Layersize,int Maxsize);

   void ForwardPass(Samples Sample,int patternNum,Sizes Layersize,int phone);

   void BackwardPass(Samples Sample,double LearningRate,int slide,Sizes Layersize, int phone);

   void PrintWeights(Sizes Layersize);// print  all weights
  //
   bool ErrorTolerance(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

   double SumSquaredError(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

  int BackPropogation(TrainingExamples TraineeSamples, double LearningRate,Sizes Layersize,char* Savefile,char* TestFile, int sampleSize, int columnSize, int outputSize);

Layer   Neurons_to_chromes(  );

double MAE(TrainingExamples TraineeSamples,int temp,Sizes Layersize);


double  MAPE(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

   void SaveLearnedData(Sizes Layersize,char* filename) ;

   void LoadSavedData(Sizes Layersize,char* filename) ;

  double TestLearnedData( Sizes Layersize,char* learntData,char* TestFile, int sampleSize, int columnSize, int outputSize );

  double GenerateData( Sizes Layersize,char* learntData,char* TestFile, int sampleSize, int columnSize, int outputSize );

 
  double CountLearningData(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

    double CountTestingData(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

   void  LoadInductiveBias();

   void  ChoromesToNeurons(  Layer NeuronChrome);

 double  ForwardFitnessPass(  Layer NeuronChrome,TrainingExamples Test) ;

   bool CheckOutput(TrainingExamples TraineeSamples,int pattern,Sizes Layersize);

   double GenerateTreeData(char* TestingFile,int testsize,Sizes Layersize, char* filename,char* load)  ;

 double TestTrainingData( Sizes Layersize,char* learntData,char* TestFile, int sampleSize, int columnSize, int outputSize,  ofstream & out2 );

  double  Radial(Frame Mean,Frame Variance, Frame Frames);

double  NormalisedMeanSquaredError(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

double  BP(  Layer NeuronChrome,   TrainingExamples Test, int generations) ;

   double Abs(double num)  ;

   double  MultiRadial(Sizes Layersize,Frame Mean, Frame Frames);
    };


double NeuralNetwork::Random()
{     int chance;
      double randomWeight=0;
      double NegativeWeight=0;
      chance =rand()%2;

      if(chance ==0){
      randomWeight =rand()% 100;
      return randomWeight*0.05;
       }

      if(chance ==1){
      NegativeWeight =rand()% 100;
      return NegativeWeight*0.05;
     }

}
double NeuralNetwork::Sigmoid(double ForwardOutput)
{
      double ActualOutput;
   #ifdef sigmoid 
     ActualOutput = (1.0 / (1.0 + exp(-1.0 * (ForwardOutput) ) ));
   #endif
    
   #ifdef tanh
      ActualOutput  = (exp(2 * ForwardOutput) - 1)/(exp(2 * ForwardOutput) + 1);
   #endif
    return  ActualOutput;
}

double NeuralNetwork::SigmoidS(double ForwardOutput)
{
      double ActualOutput;
  
     ActualOutput = (1.0 / (1.0 + exp(-1.0 * (ForwardOutput) ) ));
  
 
    return  ActualOutput;
}


double NeuralNetwork::Abs(double num)
{    if (num< 0)
      return num*-1;
      else return num;
}

double  NeuralNetwork:: MultiRadial(Sizes Layersize,Frame Mean, Frame Frames)
{
    double Gaussian = 0;
  

    return  Gaussian;
}

double NeuralNetwork::Radial(Frame Mean,Frame Variance, Frame Frames)
{
      /* double ActualOutput = 0;
     double Radial;
     double temp =0;

     temp = observation - mean;


     Radial = -1* (( temp * temp )/( 2 *  standarddev *standarddev));


     ActualOutput = exp(Radial);

    return  ActualOutput;  */

   double observation = Frames[0];
   double standarddev = Variance[0];
   double mean = Mean[0];

   standarddev  = Abs(standarddev);


       double ActualOutput = 0;
     double Radial;
     double temp =0;
     double temp2 = 0;
     temp = observation - mean;

     temp2 = (temp*temp)/(standarddev*standarddev);


     Radial = -0.5 * temp2;

     double temp3 = 1;

 // temp3 = 1/(sqrt(2*3.14*standarddev  ));
 // temp3 = Abs(temp3);
     ActualOutput = temp3* exp(Radial);

    //cout<<exp(Radial)<<" "<<ActualOutput<<endl;
    return  ActualOutput;
}


void NeuralNetwork::CreateNetwork(Sizes Layersize,int Maxsize)
{
         // for(int layer=0; layer < Layersize.size(); layer++)
         // cout<<Layersize[layer]  <<endl;

  int end = Layersize.size() - 1;

for(layer=0; layer < Layersize.size()-1; layer++){

      //-------------------------------------------
        //for( r=0; r < Layersize[layer]; r++)
           //nLayer[layer].Weights.push_back(vector<double> ());

        for( row = 0; row< Layersize[layer] ; row++)
          for( col = 0; col < Layersize[layer+1]; col++)
        	  nLayer[layer].Weights[row][col]=Random();    //nLayer[layer].Weights[row].push_back(1);
     //---------------------------------------------
        /*for( r=0; r < Layersize[layer]; r++)
           nLayer[layer].TransitionProb .push_back(vector<double> ());

        for( row = 0; row< Layersize[layer] ; row++)
          for( col = 0; col < Layersize[layer+1]; col++)
          nLayer[layer].TransitionProb [row].push_back(1);*/
     //---------------------------------------------
      //  for( r=0; r < Layersize[layer]; r++)
     //     nLayer[layer].WeightChange.push_back(vector<double> ());

        for( row = 0; row < Layersize[layer] ; row ++)
          for( col = 0; col < Layersize[layer+1]; col++)
        	  nLayer[layer].WeightChange[row ][col]=Random();//nLayer[layer].WeightChange[row ].push_back(0);
      //-------------------------------------------
  }

   //------context layer weight initialisation-------------
     // for(layer=1; layer < Layersize.size()-1; layer++){

		//for( r=0; r < Layersize[1]; r++)
        //  nLayer[1].ContextWeight.push_back(vector<double> ());

        for( row = 0; row < Layersize[1] ; row ++)
          for( col = 0; col < Layersize[1]; col++)
        	  nLayer[1].ContextWeight[row][col]=Random();//nLayer[1].ContextWeight[row].push_back(Random());

	  //}
   //------------------------------------------------------


    for( layer=0; layer < Layersize.size(); layer++){

      for( r  = 0; r  < Maxsize ; r  ++)
         nLayer[layer].Outputlayer.push_back(vector<double> ());

	  for( row = 0; row < Maxsize ; row ++)
		 for( col = 0; col < Layersize[layer]; col++)
            nLayer[layer].Outputlayer[row ].push_back(Random());
	 //--------------------------------------------------




			 for( r  = 0; r  < MaxVirtLayerSize ; r  ++)
         nLayer[layer].Error.push_back(vector<double> ());

	  for( row = 0; row < MaxVirtLayerSize ; row ++)
		 for( col = 0; col < Layersize[layer]; col++)
            nLayer[layer].Error[row ].push_back(0);

             //TransitionProb
	 //---------------------------------------------
     	 for( r  = 0; r  < MaxVirtLayerSize ; r  ++)
         nLayer[layer].RadialOutput.push_back(vector<double> ());

	  for( row = 0; row < MaxVirtLayerSize ; row ++)
		 for( col = 0; col < Layersize[layer]; col++)
            nLayer[layer].RadialOutput[row ].push_back(Random());
    //-------------------------------------------


     //---------------------------------------------

      for( row = 0; row < Layersize[layer] ; row ++)
          nLayer[layer ].Bias.push_back(Random());

      for( row = 0; row < Layersize[layer] ; row ++)
         nLayer[layer ].BiasChange.push_back(0);

       for( row = 0; row < Layersize[layer] ; row ++)
          nLayer[layer ].Mean.push_back(Random());

          for( row = 0; row < Layersize[layer] ; row ++)
          nLayer[layer ].StanDev.push_back(Random());

           for( row = 0; row < Layersize[layer] ; row ++)
          nLayer[layer ].MeanChange.push_back(0);

          for( row = 0; row < Layersize[layer] ; row ++)
          nLayer[layer ].StanDevChange.push_back(0);

     }
    //--------------------------------------

    for( r=0; r < Maxsize; r++)
           Output.push_back(vector<double> ());
     for( row = 0; row<  Maxsize ; row++)
          for( col = 0; col < Layersize[end]; col++)
           Output[row].push_back(0);

   for( row = 0; row < StringSize ; row ++)
         ChromeNeuron.push_back(0);

  // SaveLearnedData(Layersize, "createnetwork.txt");




}

  void NeuralNetwork::ForwardPass( Samples Sample,int slide ,Sizes Layersize,int phone)
{
      double WeightedSum = 0;
	   double ContextWeightSum = 0;
      double ForwardOutput;
    //  cout<<endl<<"slide  "<<slide<<"  ------------------------ "<<endl<<endl<<endl<<endl;


        int end = Layersize.size() - 1;

      for(int row = 0; row < Layersize[0] ; row ++)
       nLayer[0].Outputlayer[slide+1][row] = Sample.InputValues[slide][row];
    //--------------------------------------------


  //for(
  int layer=0;// layer < Layersize.size()-1; layer++){
 int y;
	 int x ;
 for(  y = 0; y< Layersize[layer+1]; y++) {
    for(  x = 0; x< Layersize[layer] ; x++){
       	WeightedSum += (nLayer[layer].Outputlayer[slide+1][x] * nLayer[layer].Weights[x][y]);}
    for(  x = 0; x< Layersize[layer+1] ; x++){
    ContextWeightSum += (nLayer[1].Outputlayer[slide][x] * nLayer[1].ContextWeight[x][y]);// adjust this line when use two hidden layers.
   //    	ContextWeightSum += (nLayer[1].Outputlayer[slide][x] );// adjust this line when use two hidden layers.
    }

       	ForwardOutput = (WeightedSum + ContextWeightSum )- nLayer[layer+1].Bias[y];
  nLayer[layer+1].Outputlayer[slide+1][y] = SigmoidS(ForwardOutput);
 // cout<<ForwardOutput<<endl;
  //getchar();
  WeightedSum = 0;
 ContextWeightSum = 0;
 }
//}//end layer

layer=1;


for(  y = 0; y< Layersize[layer+1]; y++) {
    for(  x = 0; x< Layersize[layer] ; x++){
       	WeightedSum += (nLayer[layer].Outputlayer[slide+1][x] * nLayer[layer].Weights[x][y]);
        ForwardOutput = (WeightedSum   )- nLayer[layer+1].Bias[y];}
 nLayer[layer+1].Outputlayer[slide+1][y] = Sigmoid(ForwardOutput);
 WeightedSum = 0;
 //cout<<   ForwardOutput<<endl;
 ContextWeightSum = 0;}


   //--------------------------------------------
    for(int output= 0; output < Layersize[end] ; output ++){
      Output[phone][output] = nLayer[end].Outputlayer[slide+1][output];
//cout<<Output[phone][output]<<" ";
      }
//cout<<endl;



 }




 void NeuralNetwork::BackwardPass(Samples Sample,double LearningRate,int slide,Sizes Layersize,int phone)
 {

    int end = Layersize.size() - 1;// know the end layer
    double temp = 0;
	 double sum = 0;
   int Endslide = Sample.PhoneSize ;
    //----------------------------------------
//cout<<slide<<"   ---------------------------------->>>>"<<endl;
    // compute error gradient for output neurons
 for(int output=0; output < Layersize[end]; output++) {
   nLayer[2].Error[Endslide][output] =  1 *(Sample.OutputValues[output] - Output[phone][output]);
		//Output[phone][output]*(1-Output[phone][output])
 	//cout<<nLayer[2].Error[Endslide][output]<<"            is output error"<<endl;
  }
//  for(int i = 0; i<= Sample.PhoneSize  ; i ++){
  //   for(int j = 0; j< Layersize[2] ; j ++)
   //     cout<<  nLayer[2].Error[i][j]<<" ";
    //        cout<<endl;
	  //   }

    //----------------------------------------
    // for(int layer = Layersize.size()-2; layer >= 0; layer--){
      int layer =1;
       for( x = 0; x< Layersize[layer] ; x++){  //inner layer
       for( y = 0; y< Layersize[layer+1]; y++) { //outer layer
    	temp += ( nLayer[layer+1].Error[Endslide][y] * nLayer[layer].Weights[x][y]);}

      	nLayer[layer].Error[Endslide][x] = nLayer[layer].Outputlayer[Endslide][x] * (1-nLayer[layer].Outputlayer[Endslide][x]) * temp;
   // cout<<	nLayer[layer].Error[Endslide][x]<<" is error of  layerrrr "<<layer<<" of slide "<<Endslide<<endl;
 	temp = 0.0;
     }
   // temp = 0.0;


 //}
  //cout<<" inner hidden layers error----"<<endl;
  //for(int i = 0; i<= Sample.PhoneSize  ; i ++){
    // for(int j = 0; j< Layersize[1] ; j ++)
     //  cout<<  nLayer[1].Error[i][j]<<" ";
      //      cout<<endl;
	  // }
//cout<<nLayer[1].Error[Endslide][0]<<"        eeee"<<endl;
 for( x = 0; x< Layersize[1] ; x++){  //inner layer
       for( y = 0; y< Layersize[1]; y++) { //outer layer
    	sum += ( nLayer[1].Error[slide][y] * nLayer[1].ContextWeight[x][y]);
//cout<<sum<< " is sum  : "<<nLayer[1].Error[slide][y]<<endl;

	   }
      	nLayer[1].Error[slide-1][x] = (nLayer[1].Outputlayer[slide-1][x] * (1-nLayer[1].Outputlayer[slide-1][x])) * sum;
    //	cout<<	nLayer[1].Error[slide-1][x]<<" is error  of slide "<<slide<<endl;
 	sum = 0.0;
     }
    sum = 0.0;



// do weight updates..
//---------------------------------------
  	double tmp  ;
//for( layer = Layersize.size()-2; layer != -1; layer--){
  for( x = 0; x< Layersize[0] ; x++){  //inner layer
     for( y = 0; y< Layersize[1]; y++) { //outer layer
      tmp  = (( LearningRate * nLayer[1].Error[slide][y] * nLayer[0].Outputlayer[slide][x])  );// weight change
      nLayer[0].Weights[x][y] +=  tmp - (tmp*weightdecay)  ;

  // 	cout<<nLayer[0].Weights[x][y]<<" is updated  weight of slide "<<slide<<endl;
            }

      }
  // }

//cout<<endl;

//-------------------------------------------------
//do top weight update
double seeda = 0;

 //if(Endslide ==  slide)
	 seeda =1;
	double tmpoo  ;
//for( layer = Layersize.size()-2; layer != -1; layer--){
  for( x = 0; x< Layersize[1] ; x++){  //inner layer
     for( y = 0; y< Layersize[2]; y++) { //outer layer
      tmpoo  = (( seeda*LearningRate * nLayer[2].Error[Endslide][y] * nLayer[1].Outputlayer[Endslide][x])  );// weight change
      nLayer[1].Weights[x][y] +=  tmpoo  - (tmpoo*weightdecay) ;

  	 //cout<<nLayer[1].Weights[x][y]<<" is updated  topp weight of slide "<<slide<<endl;
            }

      }
  seeda = 0;
  // }

  //-----------------------------------------------
	double tmp2  ;
//for( layer = Layersize.size()-2; layer != -1; layer--){
  for( x = 0; x< Layersize[1] ; x++){  //inner layer
     for( y = 0; y< Layersize[1]; y++) { //outer layer
      tmp2  = (( LearningRate * nLayer[1].Error[slide][y] * nLayer[1].Outputlayer[slide-1][x]));// weight change
      nLayer[1].ContextWeight[x][y] +=  tmp2 - (tmp2*weightdecay)  ;

   // cout<<nLayer[1].ContextWeight[x][y]<<" is updated  context weight of slide "<<slide<<endl;
            }

      }
  // }

//cout<<endl;




//update the bias
 double topbias =0;
 double seed = 0;

 //if(Endslide ==  slide)
	 seed =1;
         for( y = 0; y< Layersize[2]; y++){
          topbias  = ((seed* -1 * LearningRate * nLayer[2].Error[Endslide][y])  );
          nLayer[2].Bias[y] +=  topbias - (topbias*weightdecay)  ;
		   topbias =0;
		//   	cout<<nLayer[2].Bias[y]<<" is updated  top Bias for slide "<<Endslide<<endl;
		 }
              topbias = 0;
seed = 0;

   double tmp1;
   //---------------------------------------
    //for( layer = Layersize.size()-1; layer != 0; layer--){
      //  cout<<"layer"<<layer<<endl;

         for( y = 0; y< Layersize[1]; y++){
          tmp1 = ((-1 * LearningRate * nLayer[1].Error[slide][y])  );
          nLayer[1].Bias[y] +=   tmp1  - (tmp1*weightdecay) ;

        // nLayer[layer].BiasChange[y]= tmp1 ;
         //	 cout<<nLayer[1].Bias[y]<<" is updated  Bias for slide "<<slide<<endl;
        }
  // }



 }
double NeuralNetwork::CountLearningData(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{
    double count = 1;
    int total = TraineeSamples.SampleSize;
    double accepted =  temp * 1;

    double desiredoutput;
    double actualoutput;

    double Error;
    int end = Layersize.size() - 1;

    for(int pattern = 0; pattern< temp; pattern++){

    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);



    for(int output = 0; output < Layersize[end]; output++) {
       desiredoutput = TraineeSamples.Sample[pattern].OutputValues[output];
       actualoutput = Output[pattern][output];

    Desired[output] = desiredoutput;


   // cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;

     if((actualoutput >= 0)&&(actualoutput <= 0.2))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.8))
    actualoutput = 1;

      Actual[output] =  actualoutput;

       }
     int confirm = 0;
    for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]== Actual[b] )
      confirm++;
                                                 }
      if(confirm == Layersize[end])
      count++;

      confirm = 0;

     }



  return count;

}
   double NeuralNetwork::CountTestingData(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{
    double count = 1;
    int total = TraineeSamples.SampleSize;
    double accepted =  temp * 1;

    double desiredoutput;
    double actualoutput;

    double Error;
    int end = Layersize.size() - 1;

    for(int pattern = 0; pattern< temp; pattern++){

    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);



    for(int output = 0; output < Layersize[end]; output++) {
       desiredoutput = TraineeSamples.Sample[pattern].OutputValues[output];
       actualoutput = Output[pattern][output];

    Desired[output] = desiredoutput;


  //  cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;

     if((actualoutput >= 0)&&(actualoutput <= 0.5))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.5))
    actualoutput = 1;

      Actual[output] =  actualoutput;

       }
     int confirm = 0;
    for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]== Actual[b] )
      confirm++;
                                                 }
      if(confirm == Layersize[end])
      count++;

      confirm = 0;

     }



  return count;

}

 bool NeuralNetwork::ErrorTolerance(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{
    double count = 0;
    int total = TraineeSamples.SampleSize;
    double accepted =  temp * 1;

    double desiredoutput;
    double actualoutput;

    double Error;
    int end = Layersize.size() - 1;

    for(int pattern = 0; pattern< temp; pattern++){

    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);



    for(int output = 0; output < Layersize[end]; output++) {
       desiredoutput = TraineeSamples.Sample[pattern].OutputValues[output];
       actualoutput = Output[pattern][output];

    Desired[output] = desiredoutput;


 //  cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;

     if((actualoutput >= 0)&&(actualoutput <= 0.2))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.8))
    actualoutput = 1;

      Actual[output] =  actualoutput;

       }
     int confirm = 0;
    for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]== Actual[b] )
      confirm++;
                                            }
    //    cout<<confirm<< " is confirm"<< Layersize[end]<<endl;

         if(confirm == Layersize[end])
            count++;




        confirm = 0;
       // count = 1;
     }
     // cout<<count<< " is count"<<accepted<<endl;
       if(count ==accepted)
        return false;


  return true;

}



double NeuralNetwork::MAPE(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{    int end = Layersize.size() - 1;
    double Sum = 0;
    double Error=0;
    double ErrorSquared = 0;
    for(int pattern = 0; pattern< temp; pattern++){
    for(int output = 0; output < Layersize[end]; output++) {
      Error =  (TraineeSamples.Sample[pattern].OutputValues[output]  -Output[pattern][output]) / TraineeSamples.Sample[pattern].OutputValues[output];

      ErrorSquared += fabs(Error);
       }

        Sum += (ErrorSquared);
        ErrorSquared = 0;
}
  return (Sum/temp*Layersize[end]*100);


}


double NeuralNetwork::MAE(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{    int end = Layersize.size() - 1;
    double Sum = 0;
    double Error=0;
    double ErrorSquared = 0;
    for(int pattern = 0; pattern< temp; pattern++){
    for(int output = 0; output < Layersize[end]; output++) {
      Error =   (TraineeSamples.Sample[pattern].OutputValues[output]  -Output[pattern][output]) * 200  ;

      ErrorSquared += fabs(Error);
       }

        Sum += (ErrorSquared);
        ErrorSquared = 0;
}
  return Sum/temp*Layersize[end];
}

double NeuralNetwork::SumSquaredError(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{    int end = Layersize.size() - 1;
    double Sum = 0;
    double Error=0;
    double ErrorSquared = 0;
    for(int pattern = 0; pattern< temp; pattern++){
    for(int output = 0; output < Layersize[end]; output++) {
      Error =  TraineeSamples.Sample[pattern].OutputValues[output]  -  Output[pattern][output] ;
 
      ErrorSquared += (Error * Error);
       }

        Sum += (ErrorSquared);
        ErrorSquared = 0; 
}
  return sqrt(Sum/temp*Layersize[end]); 

 
}

double NeuralNetwork::NormalisedMeanSquaredError(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{   int end = Layersize.size() - 1;
    double Sum = 0;
    double Sum2 = 0;
    double Error=0;
    double ErrorSquared = 0;
      double Error2=0;
    double ErrorSquared2 = 0;
    double meany = 0;
    for(int pattern = 0; pattern< temp; pattern++){
    

      for(int slide = 0; slide < TraineeSamples.Sample[pattern].PhoneSize; slide++) {
         for(int input = 0; input < Layersize[0]; input++) {
       meany+=  TraineeSamples.Sample[pattern].InputValues[slide][input] ;}
       meany/= (Layersize[0]*TraineeSamples.Sample[pattern].PhoneSize);
      }

    for(int output = 0; output < Layersize[end]; output++) {
      Error2 =  TraineeSamples.Sample[pattern].OutputValues[output]  - meany;
      Error =  TraineeSamples.Sample[pattern].OutputValues[output]  -  Output[pattern][output] ;
      ErrorSquared += (Error * Error);
         ErrorSquared2 += (Error2 * Error2);
 

       }
      meany = 0;
        Sum += (ErrorSquared);
        Sum2 += (ErrorSquared2);
        ErrorSquared = 0;
         ErrorSquared2 = 0;}

  return Sum/Sum2;
}


void NeuralNetwork::PrintWeights(Sizes Layersize)
{
      int end = Layersize.size() - 1;

    for(int layer=0; layer < Layersize.size()-1; layer++){

  cout<<layer<<"  Weights::"<<endl<<endl;
   for(int row  = 0; row <Layersize[layer] ; row ++){
    for(int col = 0; col < Layersize[layer+1]; col++)
     cout<<nLayer[layer].Weights[row ][col]<<" ";
      cout<<endl;
                }
     cout<<endl<<layer<<" ContextWeight::"<<endl<<endl;

    for(int row  = 0; row <Layersize[1] ; row ++){
    for( int col = 0; col < Layersize[1]; col++)
      cout<<nLayer[1].ContextWeight[row ][col]<<" ";
       cout<<endl;
                }

             }


}
//-------------------------------------------------------

 void NeuralNetwork::SaveLearnedData(Sizes Layersize, char* filename)
  {

	ofstream out;
	out.open(filename);
	if(!out) {
    cout << endl << "failed to save file" << endl;
    return;
    }
   //-------------------------------
    for(int layer=0; layer < Layersize.size()-1; layer++){
        for(int row  = 0; row <Layersize[layer] ; row ++){
           for(int col = 0; col < Layersize[layer+1]; col++)
            out<<nLayer[layer].Weights[row ][col]<<" ";
             out<<endl;
              }
              out<<endl;
                  }
   //-------------------------------
	  for(int row  = 0; row <Layersize[1] ; row ++){
          for(int col = 0; col < Layersize[1]; col++)
            out<<nLayer[1].ContextWeight[row ][col]<<" ";
              out<<endl;
                }
              out<<endl;
   //--------------------------------
       // output bias.
   for( int layer=1; layer < Layersize.size(); layer++){
     for(int y = 0 ; y < Layersize[layer]; y++) {
	  out<<	nLayer[layer].Bias[y]<<"  ";
	   out<<endl;
        }

           }
           out<<endl;/*
          for( int row = 0; row < Layersize[1] ; row ++)
          out<<	nLayer[1].Mean[row]<<endl ;
          out<<endl;
           for( int row = 0; row < Layersize[1] ; row ++)
          out<<	nLayer[1].StanDev[row]<<endl ;  */
  //------------------------------
	  out.close();
//	cout << endl << "data saved" << endl;

	return;
}

 void NeuralNetwork::LoadSavedData(Sizes Layersize,char* filename)
{
 	ifstream in(filename);
    if(!in) {
    cout << endl << "failed to load file" << endl;
    return;
    }
     //-------------------------------
   for(int layer=0; layer < Layersize.size()-1; layer++){
    for(int row  = 0; row <Layersize[layer] ; row ++){
     for(int col = 0; col < Layersize[layer+1]; col++){
        in>>nLayer[layer].Weights[row ][col];
         cout<<nLayer[layer].Weights[row ][col]<<" ";
}}}
cout<<" . .......loaded "<<endl;
   //---------------------------------
	    for(int row  = 0; row <Layersize[1] ; row ++)
          for(int col = 0; col < Layersize[1]; col++)
            in>>nLayer[1].ContextWeight[row ][col] ;
   //--------------------------------
  // output bias.
     for( int layer=1; layer < Layersize.size(); layer++)
         for(int y = 0 ; y < Layersize[layer]; y++)
	       in>>nLayer[layer].Bias[y] ;

  //------------------------------
       /*  for(int row = 0; row < Layersize[1] ; row ++)
          in>>nLayer[1].Mean[row];

           for( int row = 0; row < Layersize[1] ; row ++)
          in>>nLayer[1].StanDev[row]; */

  in.close();
  // cout << endl << "data loaded for testing" << endl;

	return;
 }


bool NeuralNetwork::CheckOutput(TrainingExamples TraineeSamples,int pattern,Sizes Layersize)
{
     int end = Layersize.size() - 1;
    double desiredoutput;
    double actualoutput;


    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);


                             //Layersize[end]
  for(int output = 0; output < Layersize[end]; output++) {
    desiredoutput = TraineeSamples.Sample[pattern].OutputValues[output];
    actualoutput = Output[pattern][output];

    Desired[output] = desiredoutput;


  cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;


     if((actualoutput >= 0)&&(actualoutput <= 0.5))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.5))
    actualoutput = 1;

      Actual[output] =  actualoutput;
      }
     cout<<"---------------------"<<endl;
     for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]!= Actual[b] )
        return false;
    }
  return true;
}

double NeuralNetwork::TestLearnedData( Sizes Layersize,char* learntData,char* TestFile, int sampleSize, int columnSize, int outputSize )
{
     bool valid;
     double count = 1;
     double total;
     double accuracy;

	  Samples sample;

    TrainingExamples Test(TestFile, sampleSize,columnSize,outputSize);

    total = Test.SampleSize; //how many samples to test?

    CreateNetwork(Layersize,sampleSize );

    LoadSavedData(Layersize,SaveLearnt);

    for(int phone = 0; phone < Test.SampleSize; phone++)
    {
		sample = Test.Sample[phone];

	     int slide;

         for( slide  = 0; slide  < sample.PhoneSize  ; slide++) {
          ForwardPass( sample,slide,Layersize,phone);

		 }

 	// valid = CheckOutput( Test, phone,Layersize);

     // if(valid == true)
      //  count++;

	}

//CountTestingData
 count = CountTestingData(Test,sampleSize,Layersize);

accuracy = (count/total)* 100;
 cout<<"The sucessful count is "<<count<<" out of "<<total<<endl;
 cout<<"The accuracy of test is: " <<accuracy<<" %"<<endl;
 //  cout<<"ramstein"<<endl;
return accuracy;

 }

 double NeuralNetwork::GenerateData( Sizes Layersize,char* learntData,char* TestFile, int sampleSize, int columnSize, int outputSize )
{
     bool valid;
     double count = 1;
     double total;
     double accuracy;

	  Samples sample;

    TrainingExamples Test(TestFile, sampleSize,columnSize,outputSize);

    total = Test.SampleSize; //how many samples to test?

    CreateNetwork(Layersize,sampleSize );

    LoadSavedData(Layersize,SaveLearnt);

    for(int phone = 0; phone < Test.SampleSize; phone++)
    {
		sample = Test.Sample[phone];

	     int slide;

         for( slide  = 0; slide  < sample.PhoneSize  ; slide++) {
          ForwardPass( sample,slide,Layersize,phone);

		 }

 	// valid = CheckOutput( Test, phone,Layersize);

     // if(valid == true)
      //  count++;

	}

//CountTestingData
 count = CountTestingData(Test,sampleSize,Layersize);

accuracy = (count/total)* 100;
 cout<<"The sucessful count is "<<count<<" out of "<<total<<endl;
 cout<<"The accuracy of test is: " <<accuracy<<" %"<<endl;
 //  cout<<"ramstein"<<endl;
return accuracy;

 }


 double NeuralNetwork::TestTrainingData( Sizes Layersize,char* learntData,char* TestFile, int sampleSize, int columnSize, int outputSize,  ofstream & out2 )
{
     bool valid;
     double count = 1;
     double total;
     double accuracy;
   int end = Layersize.size() - 1;
	  Samples sample;

     TrainingExamples Test(TestFile, sampleSize,columnSize,outputSize);

     total = Test.SampleSize; //how many samples to test?

     CreateNetwork(Layersize,sampleSize );

     LoadSavedData(Layersize,SaveLearnt);

    for(int phone = 0; phone < Test.SampleSize; phone++)
    {
		sample = Test.Sample[phone];

	     int slide;

         for( slide  = 0; slide  < sample.PhoneSize  ; slide++) {
          ForwardPass( sample,slide,Layersize,phone);

		 }
	} 

   for(int pattern = 0; pattern< Test.SampleSize; pattern++){
    for(int output = 0; output < Layersize[end]; output++) {
    out2<< Output[pattern][output]   <<" "<<Test.Sample[pattern].OutputValues[output]<<" "<<fabs( fabs(Test.Sample[pattern].OutputValues[output] )-fabs( Output[pattern][output]))<<endl;
      
       }  }
out2<<endl; 



//out2<<endl; 
accuracy = SumSquaredError(Test,Test.SampleSize ,layersize);  
//out2<<" RMSE:  " <<accuracy<<endl;
 cout<<"RMSE: " <<accuracy<<" %"<<endl; 

  
 NMSE =  MAE(Test,Test.SampleSize ,layersize);

//out2<<" NMSE:  " <<NMSE<<endl;
return accuracy;
}


 int NeuralNetwork::BackPropogation(TrainingExamples TraineeSamples, double LearningRate,Sizes Layersize, char * Savefile,char* TestFile, int sampleSize, int columnSize, int outputSize)
{
      ofstream out;
	out.open("lambgod.txt");

     ofstream outt;
	outt.open("BP.txt");


     double SumErrorSquared;

      Sizes Array;

      for(int i = 0; i < 300; i++)
          Array.push_back(10);


       Array[0] = 0;

      // for(int k = 0;k < 300; k++)
    //   cout<<Array[k]<<" ";

      // cout<<endl;


     Samples sample;

     CreateNetwork(Layersize,TraineeSamples.SampleSize);
 //  PrintWeights( Layersize);
     int Id = 0;

int final=200;
     double Test=0;
      double Testall=0;
     bool Learn=true;
      int c =1;
   int temp = 30 ;
     int cycle = 0;
  for(  cycle = 0 ; cycle < 300  ; cycle++ ){

  cout<<endl<<endl<<"    "<<cycle<<"----------------------------"<<endl;
    temp +=Array[cycle ];
    for(int epoch = 0 ; epoch <  final   ; epoch++ ){


                                                 //TraineeSamples.SampleSize
     for(int phone = 0; phone < temp; phone++)
     {
          sample = TraineeSamples.Sample[phone];

         nLayer[1].Outputlayer[0][0] = 0.5;
         nLayer[1].Outputlayer[0][1] = -0.5;

         int slide;

         for( slide  = 0; slide  < sample.PhoneSize  ; slide++) {


          ForwardPass( sample,slide,Layersize,phone);


		 }

         //getchar();
  //cout<<endl<<" backward pass ------------------------"<<endl;
          for( slide  = sample.PhoneSize; slide   >=1  ; slide--){
         BackwardPass(sample,LearningRate,slide,Layersize,phone);
	 	 }

     }//phone



       SumErrorSquared = SumSquaredError(TraineeSamples,temp ,Layersize);

         cout<<epoch<< " : is Epoch    *********************    "<<endl;

        cout<<SumErrorSquared<< " : is SumErrorSquared"<<endl;

        Learn = ErrorTolerance(TraineeSamples,temp,Layersize);


      if(Learn == false )
         break;

         }//for epoch
         outt<<SumErrorSquared<<endl;
            SaveLearnedData(Layersize, Savefile);
             // Test = TestTrainingData( Layersize,Savefile,TestFile,sampleSize, columnSize, outputSize);
           cout<<" ---done after test train----"<<endl;
       // Testall = TestLearnedData( Layersize,Savefile,TestFile,  2047,columnSize,outputSize);
    cout<<" ---done after test all----"<<endl;

 out<<Test << "is tested all"<<endl;
 //out<<Testall << "is tested"<<endl;
  out<<temp<<" was temp"<<endl<<endl;
            if(Test >= 100)
           break;
            if(final== 1000)
                 break;
            if(temp>=(sampleSize-20)){
            final =1000;

            }
        c++;
 }//for cycle
cout<<" ---done----"<<endl;

     SaveLearnedData(Layersize, SaveLearnt);

   out.close();

   outt.close();
 return c;

}
Layer NeuralNetwork::  Neurons_to_chromes(  )
{ 
}

 void NeuralNetwork:: ChoromesToNeurons(  Layer NeuronChrome)
{
//#ifdef neuronlevel
int layer = 0;
int gene = 0;

	 for(int neu = 0; neu < layersize[1]; neu++ ){

		 for( int row = 0; row<  layersize[layer] ; row++){
		      nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ;
		      gene++;   }

	         nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;
	           gene++;
	 }


	      for(int neu = 0; neu < layersize[1]; neu++ ){
	          for( col = 0; col <  layersize[1]; col++)  {
	           nLayer[1].ContextWeight[neu][col]=   NeuronChrome[gene]   ;
	            gene++;} }


	 for(int neu = 0; neu < layersize[2]; neu++ ){


	 for( int row = 0; row<  layersize[layer+1] ; row++){
		      nLayer[layer+1].Weights[row][neu]=  NeuronChrome[gene]  ;
		      gene++;   }

	 nLayer[layer+2 ].Bias[neu] =   NeuronChrome[gene]  ;
	  gene++;

	 } 
}
double NeuralNetwork::ForwardFitnessPass(  Layer NeuronChrome,   TrainingExamples Test)

{
      // for(int phone = 0; phone < layersize.size(); phone++)
    // cout<<  layersize[phone]<<"   ";

    // cout<<endl<<Test.SampleSize;

      Samples sample;
      ChoromesToNeurons( NeuronChrome);
      //CreateNetwork(Layersize,Test.SampleSize);

      double SumErrorSquared = 0;
      bool Learn=true;

    for(int phone = 0; phone < Test.SampleSize; phone++)
    {
	 	sample = Test.Sample[phone];
     int slide;

         for( slide  = 0; slide  < sample.PhoneSize  ; slide++) {
            ForwardPass( sample,slide,layersize,phone);
	   }

         //for( slide  = sample.PhoneSize; slide   >=1  ; slide--){
         //     BackwardPass(sample,1,slide,layersize,phone);
		 //}
 	}

    SumErrorSquared = SumSquaredError(Test,Test.SampleSize ,layersize);

   //      Learn = ErrorTolerance(Test,Test.SampleSize,layersize);


    //  if(Learn == false )
      //   return -1;


//cout<<SumErrorSquared<<endl;


  return  SumErrorSquared ;

}

  //8888888888888888888888888888888888888888888888888888888888
  //8888888888888888888888888888888888888888888888888888888888
  //8888888888888888888888888888888888888888888888888888888888
  //8888888888888888888888888888888888888888888888888888888888
  //8888888888888888888888888888888888888888888888888888888888


double NeuralNetwork::BP(  Layer NeuronChrome,   TrainingExamples Test, int generations)

{

           ChoromesToNeurons( NeuronChrome);
      Samples sample;
      double SumErrorSquared = 0;
         bool Learn=true;


       //CreateNetwork(layersize,Test);
      // CreateNetwork(layersize,Test.SampleSize);




    for(int epoch = 0; epoch < generations; epoch++){

    for(int phone = 0; phone < Test.SampleSize; phone++)
    {
	 	sample = Test.Sample[phone];
     int slide;

         for( slide  = 0; slide  < sample.PhoneSize  ; slide++) {
            ForwardPass( sample,slide,layersize,phone);
	   }

         for( slide  = sample.PhoneSize; slide   >=1  ; slide--){
              BackwardPass(sample,1,slide,layersize,phone);
		 }
 	}



      // SumErrorSquared = SumSquaredError(TraineeSamples,temp ,Layersize);

    SumErrorSquared = SumSquaredError(Test,Test.SampleSize ,layersize);
    // cout<<  SumErrorSquared <<endl;
         Learn = ErrorTolerance(Test,Test.SampleSize,layersize);

    // Learn =  ErrorTolerance(Test,layersize,95);
 //cout<< SumErrorSquared<<endl;
      if(Learn == false )
         return -1;
    }
 ChromeNeuron = Neurons_to_chromes();
  return  SumErrorSquared ;

}


//*************************************************


//-------------------------------------------------------

class Individual{

   //  friend class GeneticAlgorithmn;
   // friend class CoEvolution;
  //    friend class CombinedEvolution;
       public:

        Layer Chrome;
        double Fitness;
        Layer BitChrome;

       public:
        Individual()
        {

        }
        void print();


      };
//***************************************************
//typedef vector <Individual> Pop ;
//class GeneticAlgorithmn:RandomNumber
  class GeneticAlgorithmn :public virtual RandomNumber  // , public virtual NeuralNetwork, public virtual TrainingExamples
  {
	//friend class Individual;
   // friend class TrainingExamples;
    //    friend class NeuralNetwork;
     //   friend class  Layers;
      //  friend class CoEvolution;
       // friend class CombinedEvolution;
       public:


   	 Individual Population[PopSize];
     int TempIndex[PopSize];

   	 Individual  NewPop[NPSize];
     int mom[PopSize];
     int list[NPSize];

              int MaxGen;


       int NumVariable;

       double BestFit;
       int BestIndex;
       int NumEval;

       int  kids;

       public:
        GeneticAlgorithmn(int stringSize)
        {
     	   NumVariable = stringSize;
     	   NumEval=0;
           BestIndex = 0;
         }
        GeneticAlgorithmn()
       {
             BestIndex = 0;
        }



       double Fitness() {return BestFit;}

       double  RandomWeights();


       double  RandomAddition();

       void PrintPopulation();


       int GenerateNewPCX(int pass,NeuralNetwork network,TrainingExamples Sample, double Mutation, int depth);

       double Objective(Layer x);

       void  InitilisePopulation();

       void Evaluate();


       double  modu(double index[]);



       // calculates the inner product of two vectors

       double  innerprod(double Ind1[],double Ind2[]);

       double RandomParents();

       double MainAlgorithm(double RUN, ofstream &out1, ofstream &out2, ofstream &out3);

       double Noise();

       void  my_family();   //here a random family (1 or 2) of parents is created who would be replaced by good individuals

       void  find_parents() ;
       void  rep_parents() ;  //here the best (1 or 2) individuals replace the family of parents
       void sort();

   };

   //-------------------------------


double GeneticAlgorithmn::RandomWeights()
{
      int chance;
      double randomWeight;
      double NegativeWeight;
      chance =rand()%2;

      if(chance ==0){
      randomWeight =rand()%  100000;
      return randomWeight*0.00005;
       }

      if(chance ==1){
      NegativeWeight =rand()% 100000;
      return NegativeWeight*-0.00005;
     }

}

double GeneticAlgorithmn::RandomAddition()
{
      int chance;
      double randomWeight;
      double NegativeWeight;
      chance =rand()%2;

      if(chance ==0){
      randomWeight =rand()% 100;
      return randomWeight*0.009;
       }

      if(chance ==1){
      NegativeWeight =rand()% 100;
      return NegativeWeight*-0.009;
     }

}



void GeneticAlgorithmn::InitilisePopulation()
    {

double x, y;

for(int row = 0; row < PopSize  ; row++)
TempIndex[row]=0;

  for(int row = 0; row < PopSize  ; row++) {
   for(int col = 0; col < NumVariable ; col++){

	//   x=randomperc();    // x is a uniform random number in (0,1)
	   //	y=(-10.0)+(5.0*x); // the formula used is y=a+(b-a)*x if y should be a random number in (a,b)
      Population[row].Chrome.push_back(RandomWeights());}
  }
  for(int row = 0; row < NPSize  ; row++) {
     for(int col = 0; col < NumVariable ; col++)
        NewPop[row].Chrome.push_back(0);

  }
  }

void GeneticAlgorithmn::Evaluate()
{
	// solutions are evaluated and best id is computed

	  Population[0].Fitness= Objective( Population[0].Chrome);
       BestFit = Population[0].Fitness;
       BestIndex = 0;

        for(int row = 0; row < PopSize  ; row++)
        {
          Population[row].Fitness= Objective( Population[row].Chrome);
          if ((MINIMIZE * BestFit) > (MINIMIZE * Population[row].Fitness))
          	{
        	  BestFit = Population[row].Fitness;
        	  BestIndex = row;
          	}
        }
}


void GeneticAlgorithmn::PrintPopulation()
{
	 for(int row = 0; row < PopSize  ; row++) {
	   for(int col = 0; col < NumVariable ; col++)
	      cout<< Population[row].Chrome[col]<<" ";
	      cout<<endl;
	}

	  for(int row = 0; row < PopSize  ; row++)
		  cout<< Population[row].Fitness<<endl;

	 cout<<" ---"<<endl;
	 cout<<BestFit<<"  "<<BestIndex<<endl;

		/*for(int row = 0; row < NPSize  ; row++) {
		   for(int col = 0; col < NumVariable ; col++)
		      cout<< NewPop[row].Chrome[col]<<" ";
		      cout<<endl;
		}*/

}


double GeneticAlgorithmn:: Objective(Layer x)
{
  int i,j,k;
  double fit, sumSCH;

  fit=0.0;

 

 // used for global optimisation etc (not used 

   

  return(fit);
}
//------------------------------------------------------------------------

void GeneticAlgorithmn:: my_family()   //here a random family (1 or 2) of parents is created who would be replaced by good individuals
{
  int i,j,index;
  int swp;
  double u;

  for(i=0;i<PopSize;i++)
    mom[i]=i;

  for(i=0;i<family;i++)
    {
   //   u=randomperc();
  //    index=(u*(PopSize-i))+i;

	  index = (rand()%PopSize) +i;

	 // cout<<"is index  "<<index<<endl;
      if(index>(PopSize-1)) index=PopSize-1;
      swp=mom[index];
      mom[index]=mom[i];
      mom[i]=swp;
    }
}

void GeneticAlgorithmn::find_parents()   //here the parents to be replaced are added to the temporary sub-population to assess their goodness against the new solutions formed which will be the basis of whether they should be kept or not
{
  int i,j,k;
  double u,v;

 my_family();
//cout<<kids<<endl;
  for(j=0;j<family;j++)
    {
      for(i=0;i<NumVariable;i++)
 	NewPop[kids+j].Chrome[i] = Population[mom[j]].Chrome[i];

      NewPop[kids+j].Fitness = Objective(NewPop[kids+j].Chrome);

    }
}



void GeneticAlgorithmn::rep_parents()   //here the best (1 or 2) individuals replace the family of parents
{
  int i,j;
  for(j=0;j<family;j++)
    {
      for(i=0;i<NumVariable;i++)
       Population[mom[j]].Chrome[i]=NewPop[list[j]].Chrome[i];

      Population[mom[j]].Fitness = Objective(Population[mom[j]].Chrome);

    }
}


void GeneticAlgorithmn::sort()

{
  int i,j, temp;
  double dbest;

  for (i=0;i<(kids+family);i++) list[i] = i;

  if(MINIMIZE)
    for (i=0; i<(kids+family-1); i++)
      {
	dbest = NewPop[list[i]].Fitness;
	for (j=i+1; j<(kids+family); j++)
	  {
	    if(NewPop[list[j]].Fitness < dbest)
	      {
		dbest = NewPop[list[j]].Fitness;
		temp = list[j];
		list[j] = list[i];
		list[i] = temp;
	      }
	  }
      }
  else
    for (i=0; i<(kids+family-1); i++)
      {
	dbest = NewPop[list[i]].Fitness;
	for (j=i+1; j<(kids+family); j++)
	  {
	    if(NewPop[list[j]].Fitness > dbest)
	      {
		dbest = NewPop[list[j]].Fitness;
		temp = list[j];
		list[j] = list[i];
		list[i] = temp;
	      }
	  }
      }
}


//---------------------------------------------------------------------
double GeneticAlgorithmn::  modu(double index[])
{
  int i;
  double sum,modul;

  sum=0.0;
  for(i=0;i<NumVariable ;i++)
    sum+=(index[i]*index[i]);

  modul=sqrt(sum);
  return modul;
}

// calculates the inner product of two vectors
double GeneticAlgorithmn::  innerprod(double Ind1[],double Ind2[])
{
  int i;
  double sum;

  sum=0.0;

  for(i=0;i<NumVariable ;i++)
    sum+=(Ind1[i]*Ind2[i]);

  return sum;
}

int GeneticAlgorithmn::GenerateNewPCX(int pass,NeuralNetwork network,TrainingExamples Sample, double Mutation, int depth)
{
  int i,j,num,k;
  double Centroid[NumVariable];
  double tempvar,tempsum,D_not,dist;
  double tempar1[NumVariable];
  double tempar2[NumVariable];
  double D[RandParent];
  double d[NumVariable];
  double diff[RandParent][NumVariable];
  double temp1,temp2,temp3;
  int temp;

  for(i=0;i<NumVariable;i++)
    Centroid[i]=0.0;

  // centroid is calculated here
  for(i=0;i<NumVariable;i++)
    {
      for(j=0;j<RandParent;j++)
	Centroid[i]+=Population[TempIndex[j]].Chrome[i];

      Centroid[i]/=RandParent;
      
  //cout<<Centroid[i]<<" --- "<<RandParent<<"  ";
          // if(isnan(Centroid[i])) return 0;
    }
  //cout<<endl;

  // calculate the distace (d) from centroid to the index parent arr1[0]
  // also distance (diff) between index and other parents are computed
  for(j=1;j<RandParent;j++)
    {
      for(i=0;i<NumVariable;i++)
	{
	  if(j == 1)
	    d[i]=Centroid[i]-Population[TempIndex[0]].Chrome[i];
	  diff[j][i]=Population[TempIndex[j]].Chrome[i]-Population[TempIndex[0]].Chrome[i];
	}
      if (modu(diff[j]) < EPSILON)
	{
	  cout<< "RUN Points are very close to each other. Quitting this run   " <<endl;

	  return (0);
	}

 if (isnan(diff[j][i])  )
	{
	  cout<< "`diff nan   " <<endl;
             diff[j][i] = 1;
	  return (0);
	} 


    }
  dist=modu(d); // modu calculates the magnitude of the vector

  if (dist < EPSILON)
    {
	  cout<< "RUN Points are very close to each other. Quitting this run    " <<endl;

      return (0);
    }

  // orthogonal directions are computed (see the paper)
  for(i=1;i<RandParent;i++)
    {
      temp1=innerprod(diff[i],d);
      if((modu(diff[i])*dist) == 0){
       cout<<" division by zero: part 1"<<endl;
         temp2=temp1/(1);
        }
      else{
      temp2=temp1/(modu(diff[i])*dist);}

      temp3=1.0-pow(temp2,2.0);
      D[i]=modu(diff[i])*sqrt(temp3);
    }

  D_not=0;
  for(i=1;i<RandParent;i++)
    D_not+=D[i];

  D_not/=(RandParent-1); //this is the average of the perpendicular distances from all other parents (minus the index parent) to the index vector

  // Next few steps compute the child, by starting with a random vector
  for(j=0;j<NumVariable;j++)
    {
      tempar1[j]=noise(0.0,(D_not*sigma_eta));
      //tempar1[j] = Noise();
      tempar2[j]=tempar1[j];
    }

  for(j=0;j<NumVariable;j++)
    {
      if ( pow(dist,2.0)==0){
     cout<<" division by zero: part 2"<<endl;
       tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/1);
   }
      else
      tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/pow(dist,2.0));
    }

  for(j=0;j<NumVariable;j++)
    tempar1[j]=tempar2[j];

  for(k=0;k<NumVariable;k++)
    NewPop[pass].Chrome[k]=Population[TempIndex[0]].Chrome[k]+tempar1[k];

 tempvar=noise(0.0,(sigma_zeta));

 // tempvar =Noise();


  for(k=0;k<NumVariable;k++){ 
      NewPop[pass].Chrome[k] += (tempvar*d[k]);
   
}

  double random = rand()%10;

Layer Chrome(NumVariable);

for(k=0;k<NumVariable;k++){
if(!isnan(NewPop[pass].Chrome[k] )){
 Chrome[k]=NewPop[pass].Chrome[k] ;
}
else 
  NewPop[pass].Chrome[k] =  RandomAddition();
  
   }

 


  return (1);
}




//------------------------------------------------------------------------
double GeneticAlgorithmn::  RandomParents()
{

	int i,j,index;
	  int swp;
	  double u;
	  int delta;

	  for(i=0;i<PopSize;i++)
	    TempIndex[i]=i;

	  swp=TempIndex[0];
	  TempIndex[0]=TempIndex[BestIndex];  // best is always included as a parent and is the index parent
	                       // this can be changed for solving a generic problem
	 TempIndex[BestIndex]=swp;

	  for(i=1;i<RandParent;i++)  // shuffle the other parents
	    {
	    //u=randomperc();
	    index=(rand()%PopSize)+i;

	    if(index>(PopSize-1)) index=PopSize-1;
	    swp=TempIndex[index];
	    TempIndex[index]=TempIndex[i];
	    TempIndex[i]=swp;
	    }


}
 

typedef vector<GeneticAlgorithmn> GAvector;

class Table{

	//  friend class  CoEvolution ;
    //  friend class CombinedEvolution;
	    public:

       //double   SingleSp[100];
       Layer   SingleSp ;



};

typedef vector<Table> TableVector;

class CoEvolution :  public  NeuralNetwork,   public virtual TrainingExamples,      public   GeneticAlgorithmn  ,public virtual RandomNumber
{

	// friend class  Table;
	// friend class  GeneticAlgorithmn;
	//friend class Individual;
    //friend class TrainingExamples;
       // friend class NeuralNetwork;
        //friend class  Layers;
     // friend class CombinedEvolution;
 //      protected:

public:

      int  NoSpecies;
    GAvector Species ;
   // GeneticAlgorithmn SingleGA;
//    Table  TableSp[NoSpeciesCons];
     bool end;
    TableVector  TableSp;

    vector<bool> NotConverged;
       Sizes SpeciesSize;
       Layer   Individual;
      Data TempTable;
int TotalEval;
     int TotalSize;
     int SingleGAsize;
     double Train;
          double Test;
     int kid;

      // public:
      // CoEvolution(int Size){
//
    //	   SpeciesSize = Size;
   //    }

    CoEvolution(){

    }

       void   MainProcedure(bool bp,   int RUN, ofstream &out1, ofstream &out2, ofstream &out3, double mutation,int depth );

     void  InitializeSpecies();
     void  EvaluateSpecies(NeuralNetwork network,TrainingExamples Sample);
     void    GetBestTable(int sp);
     void PrintSpecies();

     void   Join();
     double    ObjectiveFunc(Layer x);
     void Print();

     void   sort(int s );

     bool EndTraining() {return end;};

     void  find_parents(int s,NeuralNetwork network,TrainingExamples Sample);

     void EvalNewPop(int pass, int s,NeuralNetwork network,TrainingExamples Sample);

     void  rep_parents(int s,NeuralNetwork network,TrainingExamples Sample);

     void   EvolveSubPopulations(int repetitions,double h, NeuralNetwork network,TrainingExamples Sample,double mutation,int depth,ofstream &out1);

       };

void    CoEvolution:: InitializeSpecies()
{
           end = false;

	 for( int Sp = 0; Sp < NoSpecies; Sp++){
	     Species.push_back(0);
	     }

	 for( int Sp = 0; Sp < NoSpecies; Sp++){
	 	     NotConverged.push_back(true);
	 	     }

	   basic_seed=0.4122;
		   RUN =2;

		   seed=basic_seed+(1.0-basic_seed)*(double)((RUN)-1)/(double)MAXRUN;
		 			    	            if(seed>1.0) printf("\n warning!!! seed number exceeds 1.0");

			    			for( int s =0; s < NoSpecies; s++){

			    			Species[s].randomize(seed);

			    			}

			//   SingleGA.NumVariable = SingleGAsize;
		//	  SingleGA.InitilisePopulation();



	 TotalSize = 0;
		for( int row = 0; row< NoSpecies ; row++)
			TotalSize+= SpeciesSize[row];

		for( int row = 0; row< TotalSize ; row++)
		        Individual.push_back(0);

	 for( int s =0; s < NoSpecies; s++){
	 	Species[s].NumVariable= SpeciesSize[s];
		  Species[s].InitilisePopulation();
     }



	        TableSp.resize(NoSpecies);

             for( int row = 0; row< NoSpecies ; row++)
	            for( int col = 0; col < SpeciesSize[row]; col++)
	         	   TableSp[row].SingleSp.push_back(0);

    //for( int row = 0; row< NoSpecies ; row++)
      // for( int col = 0; col < SpeciesSize[row]; col++)
       //	   TableSp[row].SingleSp[col]=0;

}


void    CoEvolution:: PrintSpecies()
{

	 for( int s =0; s < NoSpecies; s++){
		 Species[s].PrintPopulation();
		cout<<s<<endl;
     }

}

void    CoEvolution:: GetBestTable(int CurrentSp)
{
  int Best;

		 for(int sN = 0; sN < CurrentSp ; sN++){
		    Best= Species[sN].BestIndex;

          // cout<<Best<<endl;
		  for(int s = 0; s < SpeciesSize[sN] ; s++)
		    	 TableSp[sN].SingleSp[s] = Species[sN].Population[Best].Chrome[s];
		 }

		 for(int sN = CurrentSp; sN < NoSpecies ; sN++){
			// cout<<"g"<<endl;
			Best= Species[sN].BestIndex;
			   //cout<<Best<<" ****"<<endl;
	     for(int s = 0; s < SpeciesSize[sN] ; s++)
			TableSp[sN].SingleSp[s]= Species[sN].Population[Best].Chrome[s];
				 }

}

void   CoEvolution:: Join()
{


	int index = 0;

	  for( int row = 0; row< NoSpecies ; row++){
		for( int col = 0; col < SpeciesSize[row]; col++){

			//if( ((TableSp[row].SingleSp[col]/1)!=TableSp[row].SingleSp[col]))
				//cout<<"************************************error******************"<<endl;
		  Individual[index] =  TableSp[row].SingleSp[col];
		  index++;
		}

	  }


}

void   CoEvolution:: Print()
{



	  for( int row = 0; row< NoSpecies ; row++){
		for( int col = 0; col < SpeciesSize[row]; col++){
		 cout<<TableSp[row].SingleSp[col]<<" ";   }
            cout<<endl;
	  }
            cout<<endl;

            for( int row = 0; row< TotalSize ; row++)
              cout<<Individual[row]<<" ";
            cout<<endl<<endl;
}
 
void    CoEvolution:: EvaluateSpecies(NeuralNetwork network,TrainingExamples Sample)
{

	 for( int SpNum =0; SpNum < NoSpecies; SpNum++){

		 GetBestTable(SpNum);

//---------make the first individual in the population the best

		 for(int i=0; i < Species[SpNum].NumVariable; i++)
		  TableSp[SpNum].SingleSp[i] = Species[SpNum].Population[0].Chrome[i];

		    Join();
		 Species[SpNum].Population[0].Fitness =  network.ForwardFitnessPass(Individual, Sample);//ObjectiveFunc(Individual);
		 TotalEval++;

		 Species[SpNum].BestFit = Species[SpNum].Population[0].Fitness;
		 Species[SpNum].BestIndex = 0;
		// cout<<"g"<<endl;
			 //------------do for the rest

		 for( int PIndex=0; PIndex< PopSize; PIndex++ ){


			 for(int i=0; i < Species[SpNum].NumVariable; i++)
	          TableSp[SpNum].SingleSp[i]  = Species[SpNum].Population[PIndex].Chrome[i];


		     Join();
        //   Print();

		     Species[SpNum].Population[PIndex].Fitness = network.ForwardFitnessPass(Individual, Sample);//
		     TotalEval++;
		     //   ObjectiveFunc(Individual);

		     if ((MINIMIZE * Species[SpNum].BestFit) > (MINIMIZE * Species[SpNum].Population[PIndex].Fitness))
		       {
		    	 Species[SpNum].BestFit = Species[SpNum].Population[PIndex].Fitness;
		    	 Species[SpNum].BestIndex = PIndex;
		    	//  cout<<Species[SpNum].Population[PIndex].Fitness<<endl;
		       }

         }

			// cout<< Species[SpNum].BestIndex<<endl;
		cout<<SpNum<<" -- "<<endl;
     }







}

double   CoEvolution:: ObjectiveFunc(Layer x)
{ 
 
}


void CoEvolution::find_parents(int s, NeuralNetwork network,TrainingExamples Sample)   //here the parents to be replaced are added to the temporary sub-population to assess their goodness against the new solutions formed which will be the basis of whether they should be kept or not
{
  int i,j,k;
  double u,v;

  Species[s].my_family();

  for(j=0;j<family;j++)
    {

    	  Species[s].NewPop[Species[s].kids+j].Chrome = Species[s].Population[Species[s].mom[j]].Chrome;



		 GetBestTable(s);
		 for(int i=0; i < Species[s].NumVariable; i++)
		  TableSp[s].SingleSp[i] =   Species[s].NewPop[Species[s].kids+j].Chrome[i];
		    Join();

		 Species[s].NewPop[Species[s].kids+j].Fitness =  network.ForwardFitnessPass(Individual, Sample);//ObjectiveFunc(Individual);
		 TotalEval++;
    }
}


void CoEvolution::EvalNewPop(int pass, int s,NeuralNetwork network,TrainingExamples Sample)
{

	 GetBestTable(s);
	 for(int i=0; i < Species[s].NumVariable; i++)
     TableSp[s].SingleSp[i] = 	Species[s].NewPop[pass].Chrome[i];
     Join();
  //   cout<<  Species[s].NewPop[pass].Fitness << "        pop fit"<<endl;

	 Species[s].NewPop[pass].Fitness =  network.ForwardFitnessPass(Individual, Sample);// ObjectiveFunc(Individual);
	 TotalEval++;


	  //   cout<<  Species[s].NewPop[pass].Fitness << "is new pop fit"<<endl;
}
void  CoEvolution::sort(int s)

{
  int i,j, temp;
  double dbest;

  for (i=0;i<(Species[s].kids+family);i++) Species[s].list[i] = i;

  if(MINIMIZE)
    for (i=0; i<(Species[s].kids+family-1); i++)
      {
	dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
	for (j=i+1; j<(Species[s].kids+family); j++)
	  {
	    if(Species[s].NewPop[Species[s].list[j]].Fitness < dbest)
	      {
		dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
		temp = Species[s].list[j];
		Species[s].list[j] = Species[s].list[i];
		Species[s].list[i] = temp;
	      }
	  }
      }
  else
    for (i=0; i<(Species[s].kids+family-1); i++)
      {
	dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
	for (j=i+1; j<(Species[s].kids+family); j++)
	  {
	    if(Species[s].NewPop[Species[s].list[j]].Fitness > dbest)
	      {
		dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
		temp = Species[s].list[j];
		Species[s].list[j] = Species[s].list[i];
		Species[s].list[i] = temp;
	      }
	  }
      }
}


void CoEvolution::rep_parents(int s,NeuralNetwork network,TrainingExamples Sample)   //here the best (1 or 2) individuals replace the family of parents
{
  int i,j;
  for(j=0;j<family;j++)
    {

   Species[s].Population[Species[s].mom[j]].Chrome = Species[s].NewPop[Species[s].list[j]].Chrome;

    	  GetBestTable(s);

    		 for(int i=0; i < Species[s].NumVariable; i++)
    	 TableSp[s].SingleSp[i] =   Species[s].Population[Species[s].mom[j]].Chrome[i];
    	  Join();

    	  Species[s].Population[Species[s].mom[j]].Fitness =  network.ForwardFitnessPass(Individual, Sample);//ObjectiveFunc(Individual);
    	  TotalEval++;
    }
}

void CoEvolution:: EvolveSubPopulations(int repetitions,double h, NeuralNetwork network,TrainingExamples Samples, double mutation,int depth,ofstream &out1)
{
	double tempfit;
		int count =0;
		int tag;
		kid = KIDS;
               int numspecies =0;

          
           

	for( int s =0; s < NoSpecies; s++) {
              if(NotConverged[s]==true){ 
               //  numspecies++;
               for (int r = 0; r < repetitions; r++){ 

	    		tempfit=   Species[s].Population[Species[s].BestIndex].Fitness;
	    		    Species[s].kids = KIDS;


	    	 	Species[s].RandomParents();

	 	        for(int i=0;i<	Species[s].kids;i++)
	 	   	     {
	 	   	      tag = 	Species[s].GenerateNewPCX(i, network, Samples,mutation, depth); //generate a child using PCX

	 	   	        if (tag == 0) {
                                   NotConverged[s]=false;
                                 //NewPop[pass].Chrome[k] end = true;
                                  out1<<"tag1"<<endl;
                                   break;
                              }
	 	   	     }
	 	   	          if (tag == 0) {
                                       //   end = true;
                                        out1<<"tag2"<<endl;
	 	   	        	NotConverged[s]=false;
	 	   	          }

	 	   	     for(int i=0;i<	Species[s].kids;i++)
	 	   	      EvalNewPop(i,s, network, Samples);


	 	      find_parents(s,network, Samples);  // form a pool from which a solution is to be
	 	   	                           //   replaced by the created child

	 	  	    Species[s].sort();          // sort the kids+parents by fitness
            //       sort(s);
 	 	      rep_parents(s,network, Samples);   // a chosen parent is replaced by the child


	 	  	   Species[s].BestIndex=0;

	 	  	         tempfit= Species[s].Population[0].Fitness;

	 	  	         for(int i=1;i<PopSize;i++)
	 	   			    if((MINIMIZE *    Species[s].Population[i].Fitness) < (MINIMIZE * tempfit))
	 	   			      {
	 	   				tempfit= Species[s].Population[i].Fitness;
	 	   			   Species[s].BestIndex=i;
	 	   			      }
                                 
	    		}
              
               }//r
		   //numspecies =0;
	     	}//species
 
}



 




//public GeneticAlgorithmn, public RandomNumber,


class CombinedEvolution        :    public    CoEvolution // ,      public virtual  NeuralNetwork, public virtual TrainingExamples, public virtual GeneticAlgorithmn
{

	// friend class  Table;
	// friend class  GeneticAlgorithmn;
	//friend class Individual;
  //  friend class TrainingExamples;
   //     friend class NeuralNetwork;
  //      friend class  Layers;
     //  protected:
       public:
    int TotalEval;
     int TotalSize;
     double Train;
          double Test;
          double TrainNMSE;
          double TestNMSE;
           double Error;
       	 CoEvolution NeuronLevel;
    CoEvolution WeightLevel;
 	 	 CoEvolution OneLevel;
int Cycles;
        bool Sucess;

          CombinedEvolution(){

          }

          int GetEval(){
            return TotalEval;
                  }
          double GetCycle(){
                     return Train;
                           }
          double GetError(){
                    return Test;
                          }

          double NMSETrain(){
                     return TrainNMSE;
                           }
          double NMSETest(){
                    return TestNMSE;
                          }

          bool GetSucess(){
                              return Sucess;
                                    }

       void   Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3,double mutation,double depth );
};

 
  
void    CombinedEvolution:: Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3,double mutation,double depth )
{

	 clock_t start = clock();

	            int hidden =h ;

		    int output = 2;
		    int input =  2;
 

		    int weightsize1 = (input*hidden);

		    int weightsize2 = (hidden*output);

		    int contextsize = hidden*hidden;
		    int biasize =   hidden + output;

		      const int outputsize  = output;
		      const int acousticVector  = input;

		    ofstream out;
			out.open("out.txt");
		   int gene = 1;
		    double trainpercent=0;
		    double testpercent=0;
		    int epoch;
		    double testtree;

		
		     	     TotalEval=0;
		    double H = 0;

		         TrainingExamples Samples(trainfile, trainsize,acousticVector,outputsize);
		      Samples.printData();

		         double error;

		         Sizes layersize;
		         layersize.push_back(acousticVector);
		         layersize.push_back(hidden);
		         layersize.push_back(outputsize);

		       NeuralNetwork network(layersize);
		         network.CreateNetwork(layersize,trainsize);

		        cout<<" doing-----"<<endl;

		if(bp){
		    epoch = network.BackPropogation(Samples,0.2,layersize,SaveLearnt,trainfile, trainsize,acousticVector,outputsize);//  train the network

		  //       Train = network.TestTrainingData( layersize,file,trainfile, trainsize,acousticVector,outputsize);
	//   Test =  network.TestLearnedData( layersize,file,testfile, testsize,acousticVector,outputsize); //test the network


		   cout<<Train<<"  "<<Test<<endl;
		  }


  else{
			 Sucess= false;
    
Cycles =0;
	  OneLevel.SpeciesSize.push_back(weightsize1+ weightsize2+ contextsize + biasize);
	  OneLevel.NoSpecies = 1;

	  OneLevel.InitializeSpecies(); 
      
	// OneLevel.EvaluateSpecies(network, Samples);
       
for( int s =0; s < OneLevel.NoSpecies; s++)
    	  OneLevel.NotConverged[s]=true;
cout<<" Evaluated OneLevel ----------->"<<endl;
//----------------------------------------------------------
	   for(int n = 0; n < (weightsize1+ weightsize2+ contextsize + biasize) ; n++)
	   WeightLevel.SpeciesSize.push_back(1);
	    WeightLevel.NoSpecies = weightsize1+ weightsize2+ contextsize + biasize;
	    WeightLevel.InitializeSpecies(); 
           
 	   WeightLevel.EvaluateSpecies(network, Samples);
             
	    for( int s =0; s < WeightLevel.NoSpecies; s++)
	        	 WeightLevel.NotConverged[s]=true;

	    cout<<" Evaluated WeightLevel ----------->"<<endl;
//-----------------------------------------------------------
	  for(int n = 0; n < hidden ; n++)
		  NeuronLevel.SpeciesSize.push_back(input+1);

	  for(int n = 0; n < hidden ; n++)
		  NeuronLevel.SpeciesSize.push_back( hidden);

	  for(int n = 0; n < output ; n++)
		  NeuronLevel.SpeciesSize.push_back(hidden+1);

	  NeuronLevel.NoSpecies = hidden+hidden+output; // ###################CHECK - with encoding....
          NeuronLevel.InitializeSpecies(); 
     
          NeuronLevel.EvaluateSpecies(network, Samples);
      
	    cout<<h<<' '<<NeuronLevel.NoSpecies<<endl;
      for( int s =0; s < NeuronLevel.NoSpecies; s++)
    	  NeuronLevel.NotConverged[s]=true;

      cout<<" Evaluated neuronLevel ----------->"<<endl;
    //-----------------------------------------------

 
      	                  	 

      	TotalEval=0;
      	NeuronLevel.TotalEval=0;
      	OneLevel.TotalEval=0;

      	WeightLevel.TotalEval=0;
        int count =0;
        Layer ErrorArray;
      	             //----------------------------------------------------
 
        bool end = false;

       double BestWL, BestNL, BestNetL;
        
         while(TotalEval<=maxgen){

             Layer IslandFitness; //best fitness at each island
                 for(int cyc = 0; cyc < 1 ; cyc++) {
	    	  WeightLevel.EvolveSubPopulations(1, 1 ,network, Samples,mutation,0,out1);
	    	 
	    	  WeightLevel.GetBestTable(WeightLevel.NoSpecies-1);
      	      WeightLevel.Join();
      	         network.ChoromesToNeurons(WeightLevel.Individual);
          network.SaveLearnedData(layersize, SaveLearnt);
          
       //double   TrainFP =   network.ForwardFitnessPass(WeightLevel.Individual, Samples);
      	               
      	          Error =WeightLevel.Species[WeightLevel.NoSpecies-1].Population[WeightLevel.Species[WeightLevel.NoSpecies-1].BestIndex].Fitness;
                    //if(count%20==0)
      	           
      	      //   Train =  network.TestTrainingData( layersize,SaveLearnt,trainfile, trainsize,acousticVector,outputsize,out2);
      	     //    out1<<hidden<<" wl "<<Train<<"    "<<Error<<"             ---"<< WeightLevel.TotalEval<<"    "<<count<<endl;
      	        	             
                  //TotalEval=WeightLevel.TotalEval; 
                  
                 // IslandFitness.push_back(Error);
			  }                    
                 BestWL=   Error;
                  
                 //Layer BestWeightLevel =  WeightLevel.Individual;
                    	        	      	      	                  	
 
             //   NeuronLevel.TotalEval=TotalEval;  
                      
 //-------------------------------------------------------------------------------------------
      	      for(int cyc = 0; cyc < hidden ; cyc++) {
						
      	         NeuronLevel.EvolveSubPopulations(1, 1 ,network, Samples,mutation,0,out1); 
      	           
      	         NeuronLevel.GetBestTable(NeuronLevel.NoSpecies-1);
      	     NeuronLevel.Join();
      	       network.ChoromesToNeurons(NeuronLevel.Individual);
         network.SaveLearnedData(layersize, SaveLearnt);
        // double TrainWL =   network.ForwardFitnessPass(NeuronLevel.Individual, Samples);
      	           
      	             Error = NeuronLevel.Species[NeuronLevel.NoSpecies-1].Population[NeuronLevel.Species[NeuronLevel.NoSpecies-1].BestIndex].Fitness;
      	         //   if(count%20==0)
      	 //  Train =  network.TestTrainingData( layersize,SaveLearnt,trainfile, trainsize,acousticVector,outputsize,out2);
      	    
      	//  out1<<hidden<<" nl "<<Train<<"    "<<Error<<"               ** " <<NeuronLevel.TotalEval<<"    "<<count<<endl;
      	       }
                // TotalEval=    NeuronLevel.TotalEval;
                 
                // IslandFitness.push_back(Error);
                 
			  TotalEval=WeightLevel.TotalEval + NeuronLevel.TotalEval  ; 
      	          
      	          
      	        
                 BestNL=   Error; 
                 
                  //Layer BestNeuronLevel =  NeuronLevel.Individual; 
      	 
                          //-----------------------------------------------------------------         
      	         
      	      /*   OneLevel.TotalEval=TotalEval; 

      	        OneLevel.EvolveSubPopulations( WeightLevel.NoSpecies-1 , 1 ,network, Samples,mutation,0,out1);
      	         
                 Error= OneLevel.Species[OneLevel.NoSpecies-1].Population[OneLevel.Species[OneLevel.NoSpecies-1].BestIndex].Fitness;
      	          //  if(count%10==0)
      	        //out1<<h<<" ol "<<Train<<"    "<<Error<<"    "<< OneLevel.TotalEval<<"    "<<count<<endl;
      	     // Train =  network.TestTrainingData( layersize,SaveLearnt,trainfile, trainsize,acousticVector,outputsize,out1);
      	         TotalEval = OneLevel.TotalEval;
      	         
      	           IslandFitness.push_back(Error);
      	           
      	           BestNetL=   Error;
      	           
      	           Layer BestOneLevel =  OneLevel.Individual;  */
                      
                 count++;


       //----------------------- compare -------------------------
 
                
                   // copy best - if best is nl, then copy to wl and netl
               if( (BestWL < BestNL)){
               	                
                             out1<<"0 WL              "<<BestWL<<"   "<<WeightLevel.TotalEval<<endl; 
                                 int  m =0;
                    for(int sp = 0; sp < NeuronLevel.NoSpecies  ; sp++){
      	                 for(int col = 0; col <  NeuronLevel.SpeciesSize[sp] ; col++) { 
                     NeuronLevel.Species[sp].Population[NeuronLevel.Species[NeuronLevel.NoSpecies-1].BestIndex].Chrome[col]= WeightLevel.Individual[m];
                    NeuronLevel.Species[sp].Population[NeuronLevel.Species[NeuronLevel.NoSpecies-1].BestIndex].Fitness = BestWL;   
                        NeuronLevel.Species[NeuronLevel.NoSpecies-1].BestIndex = WeightLevel.Species[WeightLevel.NoSpecies-1].BestIndex;                 
                                         m++;
                                             }
                                          } 
                           int nltemp = NeuronLevel.TotalEval ;
                         
                          NeuronLevel.EvaluateSpecies(network, Samples);
                           NeuronLevel.TotalEval =  nltemp ;
                         
      	                 NeuronLevel.TotalEval=+ ((PopSize)*NeuronLevel.NoSpecies ); 
									  }
                                      
                      // copy best - if best is netl, then copy to nl and wl
                      
                    //if( (BestNL <= BestWL)){
					 	else{
                   
                               
                             out1<<"1 NL               "<<BestNL<<"    "<<NeuronLevel.TotalEval<<endl; 
                               int  m =0;
                    for(int sp = 0; sp < WeightLevel.NoSpecies  ; sp++){
      	                 //for(int col = 0; col <  NeuronLevel.SpeciesSize[sp] ; col++) { 
                    WeightLevel.Species[sp].Population[WeightLevel.Species[WeightLevel.NoSpecies-1].BestIndex].Chrome[0]= NeuronLevel.Individual[sp];
                    WeightLevel.Species[sp].Population[WeightLevel.Species[WeightLevel.NoSpecies-1].BestIndex].Fitness = BestNL;   
                        WeightLevel.Species[WeightLevel.NoSpecies-1].BestIndex = NeuronLevel.Species[NeuronLevel.NoSpecies-1].BestIndex;                 
                                         //m++;
                                          //   }
                                          } 
                                          
                            // WeightLevel.EvaluateSpecies(network, Samples);
      	                     //WeightLevel.TotalEval=TotalEval + ((PopSize)*WeightLevel.NoSpecies ); 
                          
									  } 
									      
  
 

 




      	               	        }
  

      out1<<       TotalEval<< "    "<< WeightLevel.TotalEval <<"   "<<NeuronLevel.TotalEval  ; 
      	    
           

     //  out2<<"Train"<<endl;
           //  double TrainNMSE = network.NormalisedMeanSquaredError(Samples,int temp,Sizes Layersize)
           Train =  network.TestTrainingData( layersize,SaveLearnt,trainfile, trainsize,acousticVector,outputsize,out2);
              TrainNMSE =       network.NMSError();
              out2<<"Test"<<endl;
             Test =  network.TestTrainingData( layersize,SaveLearnt,testfile, testsize,acousticVector,outputsize,out2); 
              TestNMSE =       network.NMSError();
             out2<<endl;
      	  	    cout<<Test<<" was test RMSE "<<endl;
      	           out1<<endl; 
  out1<<" ------------------------------ "<<h<<"  "<<TotalEval<<"  RMSE:  "<<Train<<"  "<<Test<<" NMSE:  "<<TrainNMSE<<" "<<TestNMSE<<endl;

                 out2<<" ------------------------------ "<<h<<"  "<<TotalEval<<"  "<<Train<<"  "<<Test<<endl;
  out3<<"  "<<h<<"  "<<TotalEval<<"  RMSE:  "<<Train<< "  "<<Error <<" : "<<Test<<" NMSE:  "<<TrainNMSE<<" "<<TestNMSE<<endl;


		 }

}













//---------------------------------------------------------------------------------------
int main(void)
{
	cout<<"hello"<<endl;

//	int VSize =90;

	ofstream out1;
		out1.open("1.txt");
	ofstream out2;
	     out2.open("2.txt");
	 	ofstream out3;
	 	     out3.open("3.txt");
                     ofstream out4;
	 	     out4.open("4.txt");
 
 
     for(int hidden= 3;hidden<=9;hidden+=2){
    	// for(double onelevelstop=0.05;onelevelstop>=0.05;onelevelstop-=0.015){
    double onelevelstop=0.05;
    	 Sizes EvalAverage;
    	 Layer ErrorAverage;
    	Layer CycleAverage;

        Layer NMSETrainAve;
    	Layer NMSETestAve;

    	 int MeanEval=0;
    	 double MeanError=0;
    	double MeanCycle=0;

         double NMSETrainMean=0;
    	double NMSETestMean=0;


    	 int EvalSum=0;

        double NMSETrainSum = 0;
    	double NMSETestSum = 0;

    	 double ErrorSum=0;
    	double CycleSum=0;
    	double maxrun =30;
        int success = 0;
 
          double BestRMSE =10;
          double BestNMSE =10;

      	 for(int run=1;run<=maxrun;run++){
    	  CombinedEvolution Combined;

          Combined.Procedure(false, hidden, out1, out2, out3, 0  , onelevelstop );

          if(Combined.GetSucess()){
   success++;
}
          

          EvalAverage.push_back(Combined.GetEval());
          MeanEval+=Combined.GetEval();
 
          ErrorAverage.push_back(Combined.GetError());
    	  MeanError+= Combined.GetError();

    	  CycleAverage.push_back(Combined.GetCycle() );
    	      	  MeanCycle+= Combined.GetCycle();

          NMSETrainAve.push_back(Combined.NMSETrain());
          NMSETrainMean+=Combined.NMSETrain();

           NMSETestAve.push_back(Combined.NMSETest());
          NMSETestMean+=Combined.NMSETest();

           if(Combined.NMSETest()< BestNMSE)
              BestNMSE = Combined.NMSETest();

              if(Combined.GetError()< BestRMSE)
              BestRMSE = Combined.GetError();
           //

    	               }//run

          cout<<CycleAverage.size()<<" "<<ErrorAverage.size()<< " "<<EvalAverage.size()<<endl;
    	 MeanEval=MeanEval/EvalAverage.size();
    	 MeanError=MeanError/ErrorAverage.size();
    	 MeanCycle=MeanCycle/CycleAverage.size();
         NMSETestMean/= NMSETestAve.size();
         NMSETrainMean /= NMSETrainAve.size();

          cout<<MeanCycle<<" "<< MeanError<< " "<<MeanEval<<endl;

            for(int a=0; a < NMSETrainAve.size();a++)
    	   NMSETrainSum +=	(NMSETrainAve[a]-NMSETrainMean)*(NMSETrainAve[a]-NMSETrainMean);
            NMSETrainSum/= NMSETrainAve.size();
            NMSETrainSum = sqrt(NMSETrainSum);
            NMSETrainSum = 1.96 * (NMSETrainSum/sqrt(NMSETrainAve.size()));
          //------------------------------------------------
            for(int a=0; a < NMSETestAve.size();a++)
    	   NMSETestSum +=	(NMSETestAve[a]-NMSETestMean)*(NMSETestAve[a]-NMSETestMean);
            NMSETestSum/= NMSETestAve.size();
            NMSETestSum = sqrt(NMSETestSum);
            NMSETestSum = 1.96 * (NMSETestSum/sqrt(NMSETestAve.size()));
         //-------------------------------------------------

    	   for(int a=0; a < EvalAverage.size();a++)
    	EvalSum +=	(EvalAverage[a]-MeanEval)*(EvalAverage[a]-MeanEval);

    	  EvalSum=EvalSum/ EvalAverage.size();
    	  EvalSum = sqrt(EvalSum);

    	  EvalSum = 1.96*(EvalSum/sqrt( EvalAverage.size()));
    	  for(int a=0; a < CycleAverage.size();a++)
    		  CycleSum +=	(CycleAverage[a]-MeanCycle)*(CycleAverage[a]-MeanCycle);

    	  CycleSum=CycleSum/ CycleAverage.size();
    	  CycleSum = sqrt(CycleSum);
    	  CycleSum = 1.96*(CycleSum/sqrt( CycleAverage.size()));
    	  for(int a=0; a < ErrorAverage.size();a++)
        ErrorSum +=	(ErrorAverage[a]-MeanError)*(ErrorAverage[a]-MeanError);

    	 ErrorSum=ErrorSum/ ErrorAverage.size();
    	ErrorSum = sqrt(ErrorSum);
        ErrorSum = 1.96*(ErrorSum/sqrt(ErrorAverage.size()) );  
     	out4<< " & "<<hidden<<"  &  "  <<MeanCycle<<" $ \\pm $  "<<CycleSum<<"  &  "<<MeanError<<" $ \\pm $  "<<ErrorSum<<"  &  "<<BestRMSE<<"   &   "  << NMSETrainMean   <<" $ \\pm $  "<< NMSETrainSum   <<"   &   "<< NMSETestMean  <<" $ \\pm $  "<<NMSETestSum <<"  &   "<< BestNMSE <<" \\ "<< endl;

     	//out4<< " "<<hidden<<"     "<<MeanEval*100<<" "<<EvalSum*100<<"     "<<MeanCycle*100<<" "<<CycleSum*100<<"      "<<MeanError*100<<" "<<ErrorSum*100<<" --   "<<BestRMSE*100<<"  "<<success<< "  NMSE      "  << NMSETrainMean   <<" "<< NMSETrainSum   <<"        "<< NMSETestMean  <<" "<<NMSETestSum <<" --"<< BestNMSE << endl; 


     	EvalAverage.empty();
     	ErrorAverage.empty();
     	CycleAverage.empty();
    	// }//onelevelstop
  
     }//hidden
 	out1.close();
	out2.close();
	out3.close();
         out4.close();


 return 0;

};
